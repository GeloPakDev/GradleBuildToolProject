package com.example;

public class StringUtils {
    public boolean isPositiveNumber(String str) {
        return !org.apache.commons.lang3.StringUtils.startsWith(str, "-");
    }
}