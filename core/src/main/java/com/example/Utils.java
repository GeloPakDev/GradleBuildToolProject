package com.example;

public class Utils {
    public boolean isAllPositiveNumbers(String... str) {
        StringUtils stringUtils = new StringUtils();
        boolean check = false;
        for (String string : str) {
            if (stringUtils.isPositiveNumber(string)) {
                check = true;
            } else {
                check = false;
                break;
            }
        }
        return check;
    }
}