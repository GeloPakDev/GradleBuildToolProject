import com.example.StringUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class StringUtilsTest {
    private final StringUtils stringUtils = new StringUtils();

    @Test
    void isPositiveNumber() {
        assertTrue(stringUtils.isPositiveNumber("12"));
    }
}